#!/usr/bin/env nextflow

/*
 *
 * Pipeline for analyzing the barcodes for the glabrata library.
 * Chopping them up, cluster them, and then output a table of these to 
 * analyze in R.
 * 
 */
 
// Making output directories
file("./tmp").mkdirs()
file("./output").mkdirs()
file("./reports").mkdirs()

// Where are the gzip'd fastqs from Novogene?
// This is read in on commandline as argument --reads, as a 'd string
// params.reads = 'data/dme317_CKDL200157597-1a_HC7GGBBXX_L7_{1,2}.fq.gz'

/*
 *  The Design for the glabrata amplicon, 5' to 3'
 *  AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
 *  read1 starts here, 5' to 3'
 *  NNNNNNNNNNNNNNTTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
 *  NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
 *  GCGGTGATCACTTATGGtaccgTTCGTATAAaGTaTcCTATACGAACGGTATGCGCGGTGATCACTTAT
 *  GGTACCGTTCGTATAATGTGTACTATACGAACGGTAANNNNNAANNNNNTTNNNNNTTNNNNN
 *  GGTACCGATATCAGATCTAAGCTTGAATTCGAGNNNNNNNNNNNNNN
 *  read2 is priming back to read the previous line here, revcomp
 *  AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTTG
 */

/*
 *  read1 patterns
 *  NNNNNNNNNNNNNNTTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
 *  NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
 */

/*
 * read2 patterns, by echo "" | rev | tr 'ATCG' 'tagc' 
 * NNNNNNNNNNNNNNCTCGAATTCAAGCTTAGATCTGATATCGGTACC
 * NNNNNAANNNNNAANNNNNTTNNNNNTTACCGTTCGTATAGTACACATTATACGAACGGTACC
 */

// Parameters. So it's which read, then the lines (for xargs), then args for
// itermae to use.
itermae_params = [
        [   "1", // which read to join on
            [   lines: 400000, // then a map of parameters, lines per job and args
                args: [
                    '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{6,6})(TTAATATGGA){e<=1}(?<rest>.*)$"',
                    '-o "rest > (TATCGGTACC){e<=1}(?<barcode>[ATCGN]{5,5}AA[ATCGN]{5,5}TT[ATCGN]{5,5}TT[ATCGN]{5,5}){e<=3}(ATAACTTCGT){e<=1}"',
                    '--filter "statistics.median(sample.letter_annotations[\\"phred_quality\\"])>=30"',
                    '--filter "statistics.median(barcode.letter_annotations[\\"phred_quality\\"])>=30"',
                    '--output-id "input.id" --output-seq "sample"',
                    '--output-id "input.id" --output-seq "barcode"',
                    ''].join(' ')
                ]
            ],
        [   "2",
            [   lines: 40000,  // change to two zeros less, see if it still runs as fast
                args: [
                    '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{9,9})(TCGAATTCAA){e<=1}(?<rest>.*)$"',
                    '-o "rest > (TATCGGTACC){e<=1}(?<barcode>[ATCGN]{5,5}AA[ATCGN]{5,5}AA[ATCGN]{5,5}TT[ATCGN]{5,5}){e<=3}(TTACCGTTCG){e<=1}"',
                    '--filter "statistics.median(sample.letter_annotations[\\"phred_quality\\"])>=30"',
                    '--filter "statistics.median(barcode.letter_annotations[\\"phred_quality\\"])>=30"',
                    '--output-id "input.id" --output-seq "sample"',
                    '--output-id "input.id" --output-seq "barcode"',
                    ''].join(' ')
                ]
            ]
        ]


log.info """\
Reading reads at: $params.reads
itermae parameters of: $itermae_params
"""


// Read, chop name into ID and which read it is, 1 is forward
Channel.fromPath(params.reads)
    .map{ 
        (file, run, lane, read) = (it =~ /.*_(.+)_(L\d+)_(\d+).fq/)[0]
        [ read, run+'_'+lane, it] 
        }
    .into{ input_fastq; qc_fastq }

// Run FASTQC for QC checking, this is compiled by multiqc later
process do_fastqc {
    cpus 1
    memory '8 GB'
    label 'qc'
    input:
        tuple val(read), val(id), file(fastqz) from qc_fastq
    output:
        path("${fastqz}_qc") into out_fastqc
    shell: 
        '''
        mkdir !{fastqz}_qc
        fastqc --threads !{task.cpus} -o !{fastqz}_qc !{fastqz}
        '''
}

// Compile FASTQC outputs into a report, publish
process do_multiqc {
    cpus 1
    memory '2 GB'
    label 'qc'
    publishDir 'output'
    input:
        path("*") from out_fastqc.collect()
    output:
        file("*") into publish_multiqc
    shell: 
        '''
        multiqc ./
        '''
}

input_fastq_params = input_fastq
    .join(Channel.fromList(itermae_params),by: 0)

/* Using itermae script here to chop up the reads. It's done in two steps,
 * as a short test earlier found that to be ~15% faster, at least on that day.
 * Needs more benchmarking, but intuitively that makes more sense to do.
 */

process itermae_chop {
    cpus 16
    memory '16 GB'
    label 'itermae'
    publishDir 'tmp'
    input:
        tuple val(read), val(id), file(in_fqz), val(parmz) from input_fastq_params
    output:
        tuple val(read), val(id), file("${id}_${read}_pass.sam") into chopped_reads
    shell: 
        '''
        zcat !{in_fqz} \
            | parallel --pipe -L !{parmz.lines} \
                'itermae.py !{parmz.args} -v '\
                    > !{id}_!{read}_pass.sam 2> err
        '''
}

chopped_reads_starcode_paired = chopped_reads
    .tap { chopped_reads_bartender_pre }
    .groupTuple(by: [1] )
    .map{(it[0][0]=='1') ? [it[1],it[2][0],it[2][1]] : [it[1],it[2][1],it[2][0]] }
    .combine(
        Channel.fromList( [ ['sample','XI:0'], ['barcode','XI:1'] ] )
        )
//.subscribe{println it}

process pair_up_each_tag_starcode { 
    cpus 4
    memory '16 GB'
    label 'munge'
    publishDir 'tmp'
    input:
        tuple val(id), file(r1), file(r2), 
            val(code_name), val(code_tag) from chopped_reads_starcode_paired
    output:
        tuple val(id), val(code_name), file("${id}_${code_name}_starcode_codes") into first_paired_codes_starcode
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 16G"
        join -t'	' -j 1 \
            <( grep "!{code_tag}" !{r1} | cut -f1,10 | sort ${SORTPARM} -k1,1 ) \
            <( grep "!{code_tag}" !{r2} | cut -f1,10 | sort ${SORTPARM} -k1,1 ) \
            > !{id}_!{code_name}_starcode_codes
        '''
}

each_paired_starcode = first_paired_codes_starcode
    .groupTuple(by: [0] )
    .map{(it[1][0]=='sample') ? [it[0],it[2][0],it[2][1]] : [it[0],it[2][1],it[2][0]] }
//.subscribe{println it}

process pair_reads_split_starcode { 
    publishDir 'tmp'
    cpus 1
    memory '30 GB'
    label 'munge'
    input:
        tuple val(id), file(r1), file(r2) from each_paired_starcode
    output:
        tuple val(id), file("${id}_starcode_codes") into paired_codes_starcode_pre
    shell:
        '''
        join -t'	' -j 1 !{r1} !{r2} > !{id}_starcode_codes
        '''
}

paired_codes_starcode = paired_codes_starcode_pre
    .combine( [
            [read: 'r1', code_name: 'XI0', column: 2], 
            [read: 'r2', code_name: 'XI0', column: 3], 
            [read: 'r1', code_name: 'XI1', column: 4], 
            [read: 'r2', code_name: 'XI1', column: 5]
        ])
//    .subscribe{println it}

/*
 * Here's for clustering by starcode
 */

process starcode { 
    publishDir 'tmp'
    cpus 16
    memory '60 GB'
    label 'starcode'
    input:
        tuple val(id), file(codes), val(parmz) from paired_codes_starcode
    output:
        tuple val(id), val("${parmz.read}"), val("${parmz.code_name}"), 
            file("${id}_${parmz.read}_${parmz.code_name}_starcode_clustered") into clustered_codes_starcode
    shell:
        '''
        cat !{codes} \
            | cut -f!{parmz.column} \
            | starcode --threads !{task.cpus} --cluster-ratio 5.0 --seq-id \
            > !{id}_!{parmz.read}_!{parmz.code_name}_starcode_clustered
        '''
}

clustered_codes_pairs_starcode = clustered_codes_starcode
    .groupTuple(by: [0,1] )
    .map{(it[2][0]=='XI0') ? [it[0],it[1],it[3][0],it[3][1]] : 
            [it[0],it[1],it[3][1],it[3][0]] }
//.subscribe{println it}

process unfold_starcode {
    publishDir 'tmp'
    cpus 8
    memory '30 GB'
    label 'munge'
    input:
        tuple val(id), val(read), file(xi0_clust), file(xi1_clust) from clustered_codes_pairs_starcode
    output:
        tuple val(id), val(read), file("xi0.tsv"), file("xi1.tsv") into unfolded_codes_pairs_starcode
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 30G"
        cat !{xi0_clust} | cut -f1,3 \
            | gawk '{ split($2,a,","); for (i in a){ print a[i]","$1;} }' \
            | sort ${SORTPARM} -k1,1 \
            > xi0.tsv
        cat !{xi1_clust} | cut -f1,3 \
            | gawk '{ split($2,a,","); for (i in a){ print a[i]","$1;} }' \
            | sort ${SORTPARM} -k1,1 \
            > xi1.tsv
        '''
}

process reassemble_each_starcode {
    publishDir 'tmp'
    cpus 8
    memory '30 GB'
    label 'munge'
    input:
        tuple val(id), val(read), file(xi0_clust), file(xi1_clust) from unfolded_codes_pairs_starcode
    output:
        tuple val(id), val(read), file("${read}_starcode_clustered.tsv") into reassembled_each_starcode
    shell:
        '''
        join -j 1 -t, !{xi0_clust} !{xi1_clust} > !{read}_starcode_clustered.tsv
        '''
}

reassembled_pairs_starcode = reassembled_each_starcode
    .groupTuple(by: [0] )
    .map{ (it[1][0]=='r1') ? [it[0],it[2][0],it[2][1]] : [it[0],it[2][1],it[2][0]] }
//.subscribe{println it}

process reassemble_all {
    cpus 16
    memory '32 GB'
    label 'munge'
    publishDir 'tmp'
    input:
        tuple val(id), file(r1), file(r2) from reassembled_pairs_starcode
    output:
        tuple file("all_observations_starcode.csv"), 
            file("all_observations_starcode_counted.csv") into observations_starcode
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 30G"
        join -j 1 -t, !{r1} !{r2} \
            | tee all_observations_starcode.csv \
            | cut -d, -f 2-5 \
            | sort ${SORTPARM} \
            | uniq -c \
            | sed 's/^\\s\\+\\([[:digit:]]\\+\\)\\s\\+/\\1,/g' \
            > all_observations_starcode_counted.csv
        '''
}

process analyze_observations_starcode {
    cpus 16
    memory '32 GB'
    label 'r'
    publishDir 'output'
    input:
        file("analyze_observations_starcode.R") from Channel.fromPath("scripts/analyze_observations.R")
        tuple file(all_obs), file(counted_obs) from observations_starcode
        file(sample_sheet) from Channel.fromPath('data/dme317_sample_codes.csv')
        file(known_barcodes) from Channel.fromPath('data/known_barcodes.csv')
    output:
        file("analyze_observations_starcode.html") into starcode_report
    shell:
        '''
        Rscript -e "knitr::spin('analyze_observations_starcode.R')" !{counted_obs} !{sample_sheet} !{known_barcodes}
        '''
}

/*
 * Here's for clustering by bartender
 */


// Assembling input channel, combining running it for each code, 
// with bartender parameters 
chopped_reads_bartender = chopped_reads_bartender_pre
    .combine([ 
            [name: 'XI0', match: 'XI:0', bartender_parm: '-c 1 -z 5 -d 5 -l 3'], 
            [name: 'XI1', match: 'XI:1', bartender_parm: '-c 1 -z 5 -d 5 -l 5']
        ])
//chopped_reads_bartender.subscribe{println it}


process bartender {
    cpus 16
    memory '60 GB'
    label 'bartender'
    publishDir 'tmp'
    input:
        tuple val(read), val(id), file(sam), val(parms) from chopped_reads_bartender
    output:
        tuple val(id), val(read), val("${parms.name}"), 
            file("${id}_${read}_${parms.name}_for_bartender"), 
            file("${id}_${read}_${parms.name}_barcode.csv"), 
            file("${id}_${read}_${parms.name}_cluster.csv") into bartendered_codes
    shell:
        '''
        gawk '{if ($12=="!{parms.match}") {print $10","$1 }}' !{sam} \
            > !{id}_!{read}_!{parms.name}_for_bartender
        bartender_single_com -t !{task.cpus} !{parms.bartender_parm} \
                -f !{id}_!{read}_!{parms.name}_for_bartender \
                -o !{id}_!{read}_!{parms.name} 
        '''
}

process unfold_bartender {
    cpus 16
    memory '60 GB'
    label 'munge'
    publishDir 'tmp'
    input:
        tuple val(id), val(read), val(code_name), 
            file(input), file(barcodes), file(clusters) from bartendered_codes
    output:
        tuple val(id), val(read), val(code_name), 
            file("${id}_${read}_${code_name}_bartender_clustered") into bartendered_codes_unfold
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 20G"
        join -t, -1 3 -2 1 \
            <( tail -n+2 !{barcodes} | sort ${SORTPARM} -t, -k3,3 ) \
            <( tail -n+2 !{clusters} | sort ${SORTPARM} -t, -k1,1 ) \
            | cut -d, -f2,4 \
            | sort ${SORTPARM} -t, -k1,1 \
            | join -t, -j 1 - <( sort ${SORTPARM} -t, -k1,1 !{input} ) \
            | gawk -F, '{print $3","$2}' \
            | sort ${SORTPARM} -t, -k1,1 \
            > !{id}_!{read}_!{code_name}_bartender_clustered
        '''
}

bartendered_codes_pairs = bartendered_codes_unfold
    .groupTuple(by: [0,1] )
    .map{(it[2][0]=='XI0') ? [it[0],it[1],it[3][0],it[3][1]] : 
            [it[0],it[1],it[3][1],it[3][0]] }
//.subscribe{println it}

process reassemble_bartender {
    cpus 8
    memory '30 GB'
    label 'munge'
    publishDir 'tmp'
    input:
        tuple val(id), val(read), file(xi0_clust), file(xi1_clust) from bartendered_codes_pairs
    output:
        tuple val(id), val(read), file("${read}_bartender_clustered.tsv") into reassembled_bartender_first
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 60G"
        join -j 1 -t, !{xi0_clust} !{xi1_clust} \
            > !{read}_bartender_clustered.tsv
        '''
}

reassembled_bartender_paired = reassembled_bartender_first
    .groupTuple(by: [0] )
    .map{ (it[1][0]=='1') ? [it[0],it[2][0],it[2][1]] : [it[0],it[2][1],it[2][0]] }
//.subscribe{println it}

process reassemble_bartender_all {
    publishDir 'tmp'
    cpus 16
    memory '32 GB'
    label 'munge'
    input:
        tuple val(id), file(r1), file(r2) from reassembled_bartender_paired
    output:
        tuple file("all_observations_bartender.csv"), 
            file("all_observations_bartender_counted.csv") into observations_bartender
    shell:
        '''
        SORTPARM="--parallel=!{task.cpus} -T ./ -S 30G"
        join -j 1 -t, !{r1} !{r2} \
            | tee all_observations_bartender.csv \
            | cut -d, -f 2-5 \
            | sort ${SORTPARM} \
            | uniq -c \
            | sed 's/^\\s\\+\\([[:digit:]]\\+\\)\\s\\+/\\1,/g' \
            > all_observations_bartender_counted.csv
        '''
}

process analyze_observations_bartender {
    cpus 16
    memory '32 GB'
    label 'r'
    publishDir 'output'
    input:
        file("analyze_observations_bartender.R") from Channel.fromPath("scripts/analyze_observations.R")
        tuple file(all_obs), file(counted_obs) from observations_bartender
        file(sample_sheet) from Channel.fromPath('data/dme317_sample_codes.csv')
        file(known_barcodes) from Channel.fromPath('data/known_barcodes.csv')
    output:
        file("analyze_observations_bartender.html") into report_bartender
    shell:
        '''
        Rscript -e "knitr::spin('analyze_observations_bartender.R')" !{counted_obs} !{sample_sheet} !{known_barcodes}
        '''
}

