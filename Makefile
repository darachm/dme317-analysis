# Phony rules tells make to just ignore these fake files
.PHONY: all archive nextflow clean lightclean

# This makes the nextflow executable for running
scripts/nextflow: 
	curl -s https://get.nextflow.io | bash
	mv nextflow scripts/nextflow

# This rule calls the actual nextflow pipeline to run
nextflow: scripts/nextflow scripts/run_pipeline.nf scripts/run_pipeline.nfconfig
	unset SINGULARITY_CACHEDIR && \
	$< run $(word 2,$^) -c $(word 3,$^)  \
		--reads 'data/dme317*{1,2}.fq.gz' \
		-resume -ansi-log false -with-dag reports/dag.html
# Unset the singularity cache so we can just use it 
# a local cache, I think
# No ansi-log so we can debug easier (or it'll hide dirs)
# Makes a pretty DAG

# For archiving, this makes a really compressed zip
archive: data.zip
data.zip: data
	zip -9 -r $@ $<

# This is specific for just mysql16, backs it up to the
# RAID1 diskset
backup_arkive:
	rsync -auv ./ /mnt/archive/zppi

all: 
	@echo "none"

lightclean:
	@echo "do a light clean? if not cancel cntrl D"
	@read DO_IT
	rm -rf scripts/.ipynb_checkpoints
	rm -rf scripts/*.png
	rm -rf scripts/*.pdf
	rm -rf scripts/*.jpeg
	rm -rf scripts/*_cache
	rm -rf scripts/*_files

clean: lightclean
	@echo "do a deeper clean? if not cancel cntrl D"
	@read DO_IT
	rm -rf tmp
	rm -rf .nextflow
	rm -rf work
	rm -rf reports
